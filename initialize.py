#!/usr/bin/env python3

from django.core import management
from django.db import connection
from datetime import datetime
import os, os.path, sys


# ensure the user really wants to do this
areyousure = input('''
  You are about to drop and recreate the entire database.
  All data are about to be deleted.  Use of this script
  may cause itching, vertigo, dizziness, tingling in
  extremities, loss of balance or coordination, slurred
  speech, temporary zoobie syndrome, longer lines at the
  testing center, changed passwords in Learning Suite, or
  uncertainty about whether to call your professor
  'Brother' or 'Doctor'.

  Please type 'yes' to confirm the data destruction: ''')
if areyousure.lower() != 'yes':
    print()
    print('  Wise choice.')
    sys.exit(1)

# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'project3.settings'
import django
django.setup()


# drop and recreate the database tables
print()
print('Living on the edge!  Dropping the current database tables.')
with connection.cursor() as cursor:
    cursor.execute("DROP SCHEMA public CASCADE")
    cursor.execute("CREATE SCHEMA public")
    cursor.execute("GRANT ALL ON SCHEMA public TO postgres")
    cursor.execute("GRANT ALL ON SCHEMA public TO public")

# make the migrations and migrate
management.call_command('makemigrations')
management.call_command('migrate')


# imports for our project
from homepage import models as hmod
from decimal import Decimal
import csv
from django.contrib.auth.models import Permission, Group

#DUMMY DATA!

#locations
l1 = hmod.Location()
l1.locationName = "Office 1"
l1.save()

l2 = hmod.Location()
l2.locationName = "Office 2"
l2.save()

l3 = hmod.Location()
l3.locationName = "Office 3"
l3.save()


#manufacturer
m1 = hmod.Manufacturer()
m1.manName = "Dell EMC"
m1.manAddress = "531 Digital Way"
m1.save()

m2 = hmod.Manufacturer()
m2.manName = "Cisco"
m2.manAddress = "4090 Server Drive"
m2.save()

m3 = hmod.Manufacturer()
m3.manName = "Hitachi"
m3.manAddress = "2286 Luther Pass"
m3.save()

#oraganization
o1 = hmod.Organization()
o1.orgName = "TechOps"
o1.save()

o2 = hmod.Organization()
o2.orgName = "DevOps"
o2.save()

o3 = hmod.Organization()
o3.orgName = "Brand Management"
o3.save()

#employee
e1 = hmod.Employee()
e1.firstName = "David"
e1.lastName = "Greer"
e1.email = "david.greer@org.com"
e1.position = "Backend Developer"
e1.save()

e2 = hmod.Employee()
e2.firstName = "Kat"
e2.lastName = "Healy"
e2.email = "kat.healy@org.com"
e2.position = "Project Manager"
e2.save()

e3 = hmod.Employee()
e3.firstName = "Shayla"
e3.lastName = "Gale"
e3.email = "shayla.gale@org.com"
e3.position = "Data Center Manager"
e3.save()

e4 = hmod.Employee()
e4.firstName = "Nico"
e4.lastName = "Garcia"
e4.email = "nico.garcia@org.com"
e4.position = "Quality Assurance Engineer"
e4.save()

e5 = hmod.Employee()
e5.firstName = "Jenn"
e5.lastName = "Luther"
e5.email = "jenn.luther@org.com"
e5.position = "Frontend Developer"
e5.save()

#assets
a1 = hmod.Asset()
a1.serialNum = "129AB28OLP"
a1.assetName = "Server"
a1.manPartNum = "1454372"
a1.location = l1
a1.organization = o1
a1.manufacturer = m1
a1.employee = e1
a1.description = "this is a device"
a1.date_imp = "2009-01-19"
a1.notes = "I like this device"
a1.active = True
a1.save()

a2 = hmod.Asset()
a2.serialNum = "473KL58NM3"
a2.assetName = "Switch"
a2.manPartNum = "7466583"
a2.location = l2
a2.organization = o2
a2.manufacturer = m2
a2.employee = e2
a2.description = "this is another device in CA"
a2.date_imp = "2011-05-25"
a2.notes = "Switch switch switch"
a2.active = True
a2.save()

a3 = hmod.Asset()
a3.serialNum = "3374RE28NV"
a3.assetName = "Server"
a3.manPartNum = "8384739"
a3.location = l3
a3.organization = o3
a3.manufacturer = m3
a3.employee = e3
a3.description = "this is another device in TX"
a3.date_imp = "2015-11-04"
a3.notes = "yay"
a3.active = True
a3.save()

a4 = hmod.Asset()
a4.serialNum = "3887GH58P9"
a4.assetName = "Switch"
a4.manPartNum = "3837761"
a4.location = l2
a4.organization = o1
a4.manufacturer = m3
a4.employee = e4
a4.description = "this is another device in OR"
a4.date_imp = "2008-09-03"
a4.notes = "Switch switch switch switch"
a4.active = True
a4.save()

a4 = hmod.Asset()
a4.serialNum = "37764EG832"
a4.assetName = "Switch"
a4.manPartNum = "3837761"
a4.location = l2
a4.organization = o1
a4.manufacturer = m3
a4.employee = e4
a4.description = "this is inactive"
a4.date_imp = "2008-09-03"
a4.notes = "Switch switch switch switch"
a4.active = False
a4.save()

from django.shortcuts import render,redirect
from django.core.urlresolvers import reverse
from django.db.models import Q
from django import forms
from django.conf import settings
from homepage import models as umod
from homepage import forms as pforms
from django.http import HttpResponseRedirect
import datetime

'''******************************************
*	hompage/index page
*		-Lets make a nice pretty page for this
*********************************************'''
def index(request):
	context={'because':"I like to think about django when I sleep",}
	return render(request,'index.html',context)

'''******************************************
*	hompage/asssets page
*********************************************'''
def assets(request, isActive=''):
	#filters out all of the inactive assets
	print ("is active " + isActive)
	if isActive == '1':
		assets = umod.Asset.objects.filter(active=True)

	elif isActive == '0':
		assets = umod.Asset.objects.filter(active=False)
	else:
		assets = umod.Asset.objects.all()

	#if the user submitted the search form
	if request.method == 'POST':
		form = pforms.SearchForm(request.POST)
		# checks to see if the searchFor field is valid
		if form.is_valid():
			assets = umod.Asset.objects.filter(Q(serialNum__icontains=form.cleaned_data['searchFor']) | Q(assetName__icontains=form.cleaned_data['searchFor']))
	else:
		#else generate the unsubmitted form
		form = pforms.SearchForm()

	context = {
	'assets' : assets,
	'form' : form,
	}

	return render(request,'assets.html',context)

'''******************************************
*	register device  page
*********************************************'''
def register(request):
    #process the form

	if request.method == "POST":
		form = pforms.registerDeviceForm(request.POST)
		if form.is_valid():
			asset = form.save(commit=False)
			asset.date_imp = datetime.date.today()
			asset.active = True
			asset.save()
			return HttpResponseRedirect('/homepage/assets/')

	else:
	    form = pforms.registerDeviceForm()



	#render the template
	context = {
	    'form': form,
	}

	return render(request,'register.html',context)

'''*******************************************
*	assetinfo page
*		-an isActive state of 0 tells this page not to do anything
*		-isAssets is optional url parameter and will redirct the user if set to 1
**********************************************'''
def assetinfo(request, serialNumber,isActive,toAssets=''):

    #check the url for asset serial number
    try:
        asset = umod.Asset.objects.get(serialNum=serialNumber)
    except umod.Asset.DoesNotExist:
    	return HttpResponseRedirect(reverse('assets'))

    #if is active is set to one and the asset is active
    if(isActive=='1' and asset.active==True):

    	asset.active =False
    	asset.save()

    elif (isActive=='1' and asset.active==False):

    	asset.active=True
    	asset.save()

    #redirects to the assets page if the user clicked on the activate/deactivate link on that page
    if(toAssets=='1'):
    	return HttpResponseRedirect(reverse('assets'))

    #render the template
    context = {
        'asset': asset,
    }

    return render(request, 'assetinfo.html', context)


'''*******************************************
*	editasset page
*		-serialNum is passed through the URL
*		-form is prepopulated with asset informaiton ready to edit
**********************************************'''


def editasset(request, serialNum):
    #pull all products from the DB
	try:
		asset = umod.Asset.objects.get(serialNum=serialNum)
	except umod.Asset.DoesNotExist:
		return HttpResponseRedirect(reverse('assets'))

	if request.method == "POST":
		form = pforms.editDeviceForm(request.POST, instance=asset)
		if form.is_valid():
			asset = form.save(commit=False)
			asset.save()
			return redirect('/homepage/assets/')
	else:
		form = pforms.editDeviceForm(instance=asset)

    #render the template
	context = {
		'asset': asset,
		'form': form,
	}
	return render(request, 'editasset.html', context)


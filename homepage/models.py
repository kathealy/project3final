from django.db import models
from django.core.validators import MaxValueValidator

# Create your models here.
class Location(models.Model):
    locationName = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.locationName

class Manufacturer(models.Model):
    manName = models.CharField(max_length=100, null=True, blank=True)
    manAddress = models.CharField(max_length=100, null=True, blank=True)
    manPhone = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.manName

class Organization(models.Model):
    orgName = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.orgName

class Employee(models.Model):
    firstName = models.CharField(max_length=100, null=True, blank=True)
    lastName = models.CharField(max_length=100, null=True, blank=True)
    email = models.CharField(max_length=100, null=True, blank=True)
    position = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.firstName + " " + self.lastName

class Asset(models.Model):
    serialNum = models.CharField(max_length=10, primary_key=True, verbose_name="Serial Number")
    assetName = models.CharField(null=True, blank=True, max_length=30, verbose_name="Name")
    manPartNum = models.CharField(null=True, blank=True, max_length=130, verbose_name="Manufacturer Part Number")
    location = models.ForeignKey('Location')
    organization = models.ForeignKey('Organization')
    manufacturer = models.ForeignKey('Manufacturer')
    employee = models.ForeignKey('Employee')
    description = models.CharField(max_length=150, null=True, blank=True)
    date_imp = models.DateField(null=True, blank=True)
    notes = models.TextField(null=True, blank=True)
    active = models.BooleanField(default=True)

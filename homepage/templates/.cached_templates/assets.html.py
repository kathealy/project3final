# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1510859027.5719738
_enable_loop = True
_template_filename = '/Users/Jenn/Documents/MISM/531/project3/homepage/templates/assets.html'
_template_uri = 'assets.html'
_source_encoding = 'utf-8'
import django_mako_plus
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'homepage/templates/base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        form = context.get('form', UNDEFINED)
        assets = context.get('assets', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        form = context.get('form', UNDEFINED)
        assets = context.get('assets', UNDEFINED)
        __M_writer = context.writer()
        __M_writer("\n<div class='container viewcontainer'>\n  <h1>Assets:</h1>\n  <div>\n    ")
        __M_writer(str( form ))
        __M_writer('\n  </div><br>\n  <p>\n    <strong>Actions:</strong>\n    <a href="/homepage/index.register/" class="btn btn-primary">Create</a>\n  </p>\n  <p>\n    <strong>Filters:</strong>\n    <a href="/homepage/assets" class="btn btn-primary">Users</a>\n    <a href="/homepage/assets" class="btn btn-primary">Manufacturer</a>\n    <a href="/homepage/assets" class = "btn btn-primary">Location</a>\n    <a href="/homepage/assets" class = "btn btn-primary">Organization</a>\n  </p>\n\n\n\n  <table class="table table-striped users sortable">\n      <tr>\n        <th>Serial</th>\n        <th>Name</th>\n        <th>Location</th>\n        <th>Manufacturer</th>\n      </tr>\n')
        for a in assets:
            __M_writer('      <tr>\n        <td>')
            __M_writer(str( a.serialNum ))
            __M_writer('</a>\n        <td>')
            __M_writer(str( a.assetName ))
            __M_writer('</td>\n        <td>')
            __M_writer(str( a.manufacturer.manName ))
            __M_writer('</td>\n        <td>\n          <a href="/homepage/assetinfo/')
            __M_writer(str( a.serialNum ))
            __M_writer('/">Details</a> |\n          <a href="/homepage/asset/')
            __M_writer(str( a.serialNum ))
            __M_writer('/">Edit</a> |\n          <a href="/homepage/asset.deactivate/')
            __M_writer(str( a.serialNum ))
            __M_writer('/" class="delete_link">Deactivate</a>\n        </td>\n      </tr>\n')
        __M_writer('  </table>\n</div>\n\n  <!-- Delete Modal -->\n<div class="modal fade" id="deleteModal" role="dialog">\n  <div class="modal-dialog">\n\n    <!-- Modal content-->\n    <div class="modal-content">\n      <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal">&times;</button>\n        <h4 class="modal-title">Confirm</h4>\n      </div>\n      <div class="modal-body">\n        <p>Are you sure you want to deactivate the asset?</p>\n      </div>\n      <div class="modal-footer">\n        <a id="real_delete" href="" class="btn btn-danger" type="submit">Yes</a>\n        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "/Users/Jenn/Documents/MISM/531/project3/homepage/templates/assets.html", "uri": "assets.html", "source_encoding": "utf-8", "line_map": {"28": 0, "37": 1, "42": 69, "48": 4, "56": 4, "57": 8, "58": 8, "59": 31, "60": 32, "61": 33, "62": 33, "63": 34, "64": 34, "65": 35, "66": 35, "67": 37, "68": 37, "69": 38, "70": 38, "71": 39, "72": 39, "73": 43, "79": 73}}
__M_END_METADATA
"""

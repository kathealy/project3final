from django import forms
from .models import Asset

'''*******************************************
* 	Search Form
*********************************************'''
class SearchForm(forms.Form):
	searchFor = forms.CharField(label='Search', max_length = 100)


'''*****************************************
*	Register asset form
*		-This also needs to be connected to the register page
*******************************************'''
class registerDeviceForm(forms.ModelForm):

	class Meta:
		model = Asset
		fields = ('serialNum', 'assetName', 'manPartNum', 'location', 'organization', 'manufacturer', 'employee', 'description', 'notes')

	def clean_serialNum(self):
		serialNum = self.cleaned_data.get('serialNum')
		if len(serialNum) != 10:
			raise forms.ValidationError('Serial Number needs to be 10 digits')

		return serialNum


'''*****************************************
*	Edit asset form
*		-This also needs to be connected to the register page
*******************************************'''
class editDeviceForm(forms.ModelForm):

	class Meta:
		model = Asset
		fields = ('assetName', 'manPartNum', 'location', 'organization', 'manufacturer', 'employee', 'description', 'notes')

from django.conf.urls import url,include

from . import views

urlpatterns = [
	url(r'^register', views.register, name='register'),
	url(r'^assets/(?P<isActive>[0-2]{1})/$',views.assets, name='assets'),
	url(r'^assets', views.assets, name='assets'),
	url(r'^assetinfo/(?P<serialNumber>[\w\s]{10})/(?P<isActive>[0-2]{1})/$',views.assetinfo, name='assetinfo'),
	url(r'^assetinfo/(?P<serialNumber>[\w\s]{10})/(?P<isActive>[0-2]{1})/(?P<toAssets>[1]{1})/$',views.assetinfo, name='assetsRedirect'),
	url(r'^index', views.index, name='index'),
	url(r'^editasset/(?P<serialNum>[\w\s]{10})',views.editasset, name='editasset'),

]
